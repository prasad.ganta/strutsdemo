<%@ taglib uri="/WEB-INF/struts-html" prefix="html"%>

<html:html>

<body>
<html:form action="/checkLogin">
	<html:errors />

	<table>
		<tr>
			<td align="center" colspan="2"><font size="4">Please
			Login</font>
		</tr>
		<tr>
			<td align="right">Username</td>
			<td align="left"><html:text property="username" size="30"
				maxlength="30" /></td>
		</tr>
		<tr>
			<td align="right">Password</td>
			<td align="left"><html:text property="password" size="30"
				maxlength="30" /></td>
		</tr>

		<tr>
			<td align="right"><html:submit>Login</html:submit></td>
			<td align="left"><html:cancel>Cancel</html:cancel></td>
		</tr>
	</table>
</html:form>


</body>
</html:html>
