package com.sample;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class LoginFormBean extends org.apache.struts.action.ActionForm {

    String username;
    String password;
    
    public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LoginFormBean () {
    }

    public void reset(ActionMapping actionMapping, HttpServletRequest request) {
        // TODO: Write method body
        this.username = null;
        this.password = null;
    }

    public ActionErrors validate(ActionMapping actionMapping, HttpServletRequest request) {
    	ActionErrors errors = new ActionErrors();
    	
    	if( getUsername() == null || getUsername().length() < 1 ) {
            errors.add("username",new ActionMessage("username.error"));
          }
          if( getPassword() == null || getPassword().length() < 1 ) {
            errors.add("password",new ActionMessage("password.error"));
        }
          return errors;

    }


}