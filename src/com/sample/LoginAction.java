package com.sample;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

 
import com.sample.LoginDAO;
import com.sample.LoginFormBean;

public class LoginAction extends org.apache.struts.action.Action {
       
    public LoginAction() {
    }
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        LoginFormBean loginForm = (LoginFormBean)form;
        boolean validLogin = LoginDAO.login(loginForm.getUsername(),
        		                            loginForm.getPassword());
        
        if (validLogin) {
        	return mapping.findForward("welcome");     	  
        }
        else {
        	return mapping.findForward("error");      	  
        }
    }

}